/**
 * let's slide .
 * 使用前引用 Jquery
 * @author LeonHu
 * @version 0.0.1
 * @modify 2014-05-23
 */

 (function(){		
	slide(".slider",".imageBox",".icoBox","1","fade");
	slide(".r4_slide",".imageBox",".icoBox","3","");
})();

/**
 * @param {node} item 图片轮换整体容器
 * @param {Node} imageBox 图片容器
 * @param {Node} icoBox  索引小图标容器(此元素是必须添加到html的)
 * @param {number} imgWidth  图片宽度
 * @param {number} ispeed  切换间隔时间:1.2.3种速度
 * @param {number} itype  "fade"为渐显,否则为滑动
 * @returns {} 图片轮播效果
*/
function slide(item,imageBox,icoBox,ispeed,itype){
	var imageRotation = item,
		imageBox = $(imageRotation).children(imageBox)[0],
		icoBox = $(imageRotation).children(icoBox)[0],
		icoArr = $(icoBox).children(),
		// 图片宽度
		imageWidth = $(imageRotation).width(), 
		// 图片数量
		imageNum = $(imageBox).children().size(), 
		// 图片容器宽度 
		imageReelWidth = imageWidth*imageNum,  
		// 当前图片ID
		activeID = parseInt($($(icoBox).children(".active")[0]).attr("rel")),  
		// 下张图片ID
		nextID = 0,  
		// setInterval() 函数的ID
		setIntervalID,  
		// 图片动画执行速度,毫秒
		imageSpeed =500,  
		// 标题动画执行速度
		titleSpeed =250;
		// 间隔时间
	var	intervalTime = 2600;
		if(ispeed == "1")
		{
			intervalTime = 2600;
		}
		if(ispeed == "2"){
			intervalTime = 3200;
		}
		if(ispeed == "3"){
			intervalTime = 4000;
		}
		//切换方式
		var sType = itype
		// 设置 图片容器 的宽度
		$(imageBox).css({width : imageReelWidth + "px"});
		/**
		 * @param {number} clickID 点击的索引图标id
		 * @returns {} 图片滚动到指定索引
		*/
		var rotate=function(clickID){
			if(clickID){ nextID = clickID; }
			else{ nextID=activeID<=(imageNum-1) ? activeID+1 : 1; }
			// 交换图标
			$(icoArr[activeID-1]).removeClass("active");
			$(icoArr[nextID-1]).addClass("active");
			// 交换图片
			//切换方式:fade为渐显,否则为滑动
			if(sType == "fade"){
				var as = $(imageBox).children();
				var imgArr = as.children();
				$(imgArr).fadeOut(100);
				$(imgArr[nextID-1]).fadeIn(500);
			}
			else{
				$(imageBox).animate({left:"-"+(nextID-1)*imageWidth+"px"} , imageSpeed);
			}
			// 交换ID
			activeID = nextID;
		}
		//周期执行,自动轮播
		setIntervalID=setInterval(rotate,intervalTime);
		//hover时停止轮播移出继续
		$(imageBox).hover(
			function(){ clearInterval(setIntervalID); },
			function(){ setIntervalID=setInterval(rotate,intervalTime); }
		);	
		//可更改为mouseover,需限制短时间只能执行一次
		$(icoArr).click(function(){
			//停止自动轮播
			clearInterval(setIntervalID);
			var clickID = parseInt($(this).attr("rel"));
			rotate(clickID);
			//继续自动轮播
			setIntervalID=setInterval(rotate,intervalTime);
		});
}