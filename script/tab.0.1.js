/**
 * 	let's tab.js
 * @Tab选项卡切换
 * @author LeonHu
 * @version 0.0.1
 * @modify 2014-05-23
 */

//初始化tab选项卡(头部ul的id,内容div的class)
(function(){		
	btTabOn("newsHead","tab_c");
	btTabOn("m2_tab","tab_list");
})();

//隐藏元素	
function btHide(id){						
	var div = document.getElementById(id);
	if(div){
		div.style.display = "none";
	}
}

//显示元素
function btShow(id){				
	var div = document.getElementById(id);
	if(div){
		div.style.display = "block";
	}
}

/**
 * @param {Number} index Tab选项卡索引数
 * @param {Node} head 头部ul的id
 * @param {Node} divs 内容div的class
 * @returns {} 隐藏除index项以外的div内容
*/
function btTabRemove(index,head,divs)
{
	var tab_head = document.getElementById(head);
	if(tab_head){
		//head中的li集合a集合
		var lis = tab_head.getElementsByTagName("li");
		var as = tab_head.getElementsByTagName("a");
		for(var i=0; i<lis.length; i++){
			//重置所有li的class为空,去除current
			lis[i].className = "";	
			//隐藏所有div
			btHide(divs+"_"+i);
			//索引数字匹配时
			if(i == index){
				//设置class为current			
				lis[index].className = "current";
			}
		}
		//id为divs_index的div显示
		btShow(divs+"_"+index);								
	}
}

/**
 * @param {Node} head 头部ul的id
 * @param {Node} divs 内容div的class
 * @returns {} 鼠标经过head中的a[i]时,隐藏所有div内容,除了第i项
*/
function btTabOn(head,divs)	
{
	var tab_head = document.getElementById(head);
	if(tab_head){
		//默认，隐藏除0以外的所有div
		btTabRemove(0,head,divs);
		var as = tab_head.getElementsByTagName("a");	
		for(var i=0; i<as.length; i++){
			as[i].num = i;								
			as[i].onmouseover = function(){	
				//点击后把this的id传给index	
				btTabRemove(this.num,head,divs);
				return false;
			}
		}
	}
}