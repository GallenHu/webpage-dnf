/**
 * 	main.js
 * @author LeonHu
 * @modify 2014-05-23
 */

 $(document).ready(function(){
 $("#wp_r5 .link_list ul").jscroll({
   W:"13px"
  ,BgUrl:"url(../dnf/images/slt_bg.png)"
  ,Bg:"-39px 0px repeat-y"
  ,Bar:{Pos:"bottom"
        ,Bd:{Out:"#B4664B",Hover:"#B4664B"}
        ,Bg:{Out:" -26px 0px repeat-y",Hover:"-26px 0px repeat-y",Focus:"-26px 0px repeat-y"}}
        ,Btn:{btn:true
              ,uBg:{Out:"-13px 0px",Hover:"-13px 0px",Focus:"-13px 0px"}
              ,dBg:{Out:" 0px 0px",Hover:" 0px 0px",Focus:" 0px 0px"}}
  ,Fn:function(){}
 });
});